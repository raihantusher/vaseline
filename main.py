from bottle import Bottle,route,jinja2_view, request,run ,template, static_file,error,response,redirect

import functools

from pages.Student.signup import signup
from pages.Student.login import login

from pages.Instructor.admin import admin
from pages.Instructor.home import admin_home
from pages.Instructor.q_set import question_set

from pages.Student.home import student_home

from output_trace import Trace

import os

from db.config import User,Question


view = functools.partial(jinja2_view, template_lookup=['views'])

app = Bottle()


@app.route('/')
def index():
    #data={    }
    #data["user"]=request.get_cookie("user",secret='some-secret-key')
    return template("index")


@app.route('/logout',method='GET')
def logout():
    response.set_cookie("user",{},secret='some-secret-key')
    redirect("/")

@app.route('/signin')
def signin():
    return template('./instructor/login')




@app.route('/compile',method="POST")
def questoin():
    code=request.forms.get("code")
    trace=Trace(code)
    obj=trace.output_dict()
    return obj

@app.route("/sq", method="POST")
def sq():
    response.set_header('Origin', '*')
    data={}
    data["question"]=request.forms.get("question")
    data['code']=request.forms.get("code")
    data['title']=request.forms.get("title")
    data["output"]=request.forms.get("output")
    user_id=request.get_cookie("user",secret='some-secret-key')['_id']
    
    q=Question()
    if q.insert(data,user_id) is not None:
        return {"response":True}
   
    return {"response":False}


@app.route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./static/')

@app.error(404)
def error404(error):
    return template('404')




@app.route('/upload')
def upload():
    return '''
        <form action="/upload" method="post" enctype="multipart/form-data">
  Category:      <input type="text" name="category" />
  Select a file: <input type="file" name="upload" />
  <input type="submit" value="Start upload" />
</form>
    '''
    

'''
@route('/upload', method='POST')
def do_upload():
    category   = request.forms.get('category')
    upload     = request.files.get('upload')
    print (upload)
    name, ext = os.path.splitext(upload.filename)

    if ext not in ('.png','.jpg','.jpeg'):
        return 'File extension not allowed.'
    
    save_path = "hello"
    upload.save(save_path) # appends upload.filename automatically
    return 'OK'
'''


if __name__ == '__main__':
    app.merge(admin)
    app.merge(admin_home)
    app.merge(question_set)

    app.merge(student_home)
    
    app.merge(signup)
    app.merge(login)
    app.run(host='0.0.0.0', port=8000,debug=True,reloader=True)
