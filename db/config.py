from pymongo import MongoClient
from bson.objectid import ObjectId

import datetime
import bcrypt


class Connection:
    def __init__(self):
        self.client=MongoClient('localhost', 27017)
        self.db = self.client.vaseline
        #collection users
        self.users=self.db.users

class User(Connection):
    info={}
    
    def __init__(self):
        super().__init__()

        
    
    def registration(self,user={}):
        user["password"]= bcrypt.hashpw(user["password"].encode('utf-8'), bcrypt.gensalt(14))
        return self.users.insert_one(user).inserted_id

    def login(self,userID,password):
        
        user=self.users.find_one({"userid":userID})
        password=password.encode("utf-8")
        if bcrypt.checkpw(password, user["password"]):
            self.info=user
            return self.info
        else:
            return False
    def all_user(self):
        return self.users.find()
    def find(self,query):
        return self.users.find(query)
    def delete(self,query):
        return self.answers.delete_one(query)
    


        
class Question(Connection):
    def __init__(self):
        super().__init__()
        self.questions=self.db.questions
    
    def insert(self,q,user_id):
        q['user_id']=user_id
        return self.questions.insert_one(q).inserted_id
    
    def all_question(self):
        return self.questions.find()
    
    def get_one(self,id):
        return self.questions.find_one({'_id':ObjectId(id)})

    def delete(self,query):
        return self.questions.delete_one(query)

    def q_answered(self,q_id,user_id):
        return self.questions.update_one({"_id":q_id},{"$addToSet":{"users_answered":user_id}})
    def total(self,query={}):
        return self.questions.count_documents(query)

class Answer(Connection):
    def __init__(self):
        super().__init__()
        self.answers=self.db.answers
    
    def insert(self,q,user_id):
        q['user_id']=user_id
        return self.answers.insert_one(q).inserted_id
    
    def all_answers(self):
        return self.answers.find()
    
    def get_one(self,id):
        return self.answers.find_one({'_id':ObjectId(id)})

    def get_by_user(self,id):
        return self.answers.find({'user_id':ObjectId(id)})
    
    def get_by_question(self,id):
        return self.answers.find_one({'question_id':ObjectId(id)})

    
    def delete(self,query):
        return self.answers.delete_one(query)
    
    def total(self,query={}):
        return self.answers.count_documents(query)