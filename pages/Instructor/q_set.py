from bottle import Bottle,route,jinja2_view, request,template, static_file,error,response,redirect
import functools

import datetime
from db.config import User,Question

question_set=Bottle()


user=User()
questions=Question()

view = functools.partial(jinja2_view, template_lookup=['views'])
