from bottle import Bottle,route,jinja2_view, request,template, static_file,error,response,redirect
import functools

import datetime
from db.config import User,Question

admin_home=Bottle()


user=User()
questions=Question()

view = functools.partial(jinja2_view, template_lookup=['views'])

@admin_home.route('/admin_home')
@view("instructor/index")
def w():
    current_user=request.get_cookie("user",secret='some-secret-key')
    return {'user':current_user}


@admin_home.route('/admin/add-question')
@view('instructor/add_question.html')
def aq():
    current_user=request.get_cookie("user",secret='some-secret-key')
    return {'title':'Add new question!!','user':current_user}


'''
@admin_home.route('/admin_home/<p>')
def question(p):
    if(p=="add_question"):
        return template("./instructor/add_question")
    elif(p=="questions"):
        question_all=questions.all_question()
        return template("./instructor/question_list",questions=question_all)

    elif(p=="users"):
        users=user.all_user()
        q=request.query.q
        if len(q)>0:
            users=user.find({"userid":q})
            
        
        return template("./instructor/users",users=users)
'''