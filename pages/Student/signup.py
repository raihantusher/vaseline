from bottle import Bottle,route, request,run ,template, static_file,error,response,redirect
import datetime
from db.config import User

signup=Bottle()

@signup.route('/signup',method=["POST","GET"])
def registration():
    message=request.get_cookie("message",secret='some-secret-key')
    



    post_type=request.forms.get("post_type")

    user=User()

    info={}


    if(post_type=="sign_up"):
        info["fullname"]=request.forms.get("fullname")
        info["userid"]=request.forms.get("userid")
        info["email"]=request.forms.get("email")
        info["password"]=request.forms.get("password")
        info["role"]="member"
        info["data"]=datetime.datetime.utcnow()
        user.registration(info)
        response.set_cookie("message","Signup successfully complete!",secret='some-secret-key',max_age=1)
        redirect("/signup")

    return template("./instructor/signup",message=message)