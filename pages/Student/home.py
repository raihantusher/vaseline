from bottle import Bottle,route, jinja2_view, request,template, static_file,error,response,redirect
import functools

from bson.objectid import ObjectId
import json

import datetime
from db.config import User,Question,Answer

student_home=Bottle()

view = functools.partial(jinja2_view, template_lookup=['views'])

user=User()
questions=Question()
answers=Answer()

@student_home.route('/student_home')
@view('student/index.html')
def w():
    #get all question list
    q=questions.all_question()

    
    #get user details from cookie
    user=request.get_cookie("user",secret='some-secret-key')
    total=questions.total()
    right=answers.total({"correct":"right"})
    pending=answers.total({"correct":"pending"})
    wrong=answers.total({"correct":"wrong"})
    due=total-(right+pending+wrong)

    #return views with following data
    return {'title':"Student home",'question':q,'user':user,
            "total_q":total,"right_a":right,'pending_a':pending,"wrong_a":wrong,"due":due}
   


@student_home.route('/test_page/<id>')
@view('student/test_page.html')
def test(id):
    q=questions.get_one(id)
    user=request.get_cookie("user",secret='some-secret-key')

    
    return {'title':"test page",'test':q,'user':user}



@student_home.route('/view_result/<id>')
@view('student/result.html')
def result(id):
    q=questions.get_one(id)
    a=answers.get_by_question(id)
    
    user=request.get_cookie("user",secret='some-secret-key')
    return {'user':user,'test':q,"answer":a}




@student_home.route('/submit_answer',method="POST")
def sa():
    db={ }
    db["question_id"]=request.forms.get("q_id")
    db["question_id"]=ObjectId(db["question_id"])
    db["answer_code"]=request.forms.get("a_c")
    db["output"]=request.forms.get("output")
    
    user_id=request.get_cookie("user",secret='some-secret-key')['_id']
    
    db["remarks"]=""
    db["correct"]="pending"
    
    q=questions.get_one(db["question_id"])
    
    
    print(q)
    if db['output'] == q['output']:
        answers.insert(db,user_id)
        questions.q_answered(db["question_id"],ObjectId(user_id))
        return {'response':True}
    
    return {'response':False}